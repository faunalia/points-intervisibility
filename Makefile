SCRIPTNAME = visibility

package:
	# Create a zip package of the plugin named script.zip.
	# This requires use of git (your plugin development directory must be a
	# git repository).
	# To use, pass a valid commit or tag as follows:
	#   make package
	@echo
	@echo "------------------------------------"
	@echo "Exporting plugin to zip package.	"
	@echo "------------------------------------"
	rm -f $(SCRIPTNAME).zip
	git archive --prefix=$(SCRIPTNAME)/ -o $(SCRIPTNAME).zip master
	echo "Created package: $(PLUGINNAME).zip"
