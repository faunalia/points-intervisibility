# Installation

In order to install the script you have to open the Processing toolbox and click
on the python icon and select `Add Script to Toolbox`.

Browse the path where the script is installed and load it. 

It will appear in the Script provider within the `Visibility` group with the 
`Default Visibility` name.

