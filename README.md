# Script Usage

This script creates multipolygons layers from input point layer, dtm, point
elevation, radius distance and target elevation.

**IMPORTANT**: the batch mode **MUST BE ACTIVATED MANUALLY**. Just press the green arrow next to the point layer combo box.

See [INSTALL.md](INSTALL.md) for installation instructions.

### Input data

1. **Input Point layer**
2. **Elevation**: field of the input point layer (each point can have a different elevation)
3. **DTM**: raster layer of terrain elevation
4. **Distance (radius)**: in meters. The default `-1` value means full horizon. See [GRASS documentation](https://grass.osgeo.org/grass74/manuals/r.viewshed.html) for more information
5. **Target elevation**: in meters. Elevation of the target point
6. **Point ID**: field containing an **UNIQUE** ID for each point. **WARNING** the ID **MUST BE UNIQUE** for each point. You can easily add an unique ID with the `$id` function of the attribute table (see [QGIS documentation](https://docs.qgis.org/testing/en/docs/user_manual/working_with_vector/expression.html#record-and-attributes-functions))

![Dialog](img/dialog.png)

### Output data

Be sure to have activated the **iterative mode**.

A multipolygon layer **for each** input point.

Each multipolygon represent the visible area of the point with the specified parameters. The attribute table contains:

* **ID** of the input point (taken from the attribute table)
* **Elevation** of the input point (taken from the attribute table)
* **Distance (radius)**
* **Offset elevation**

![Dialog](img/result.png)

Special thanks to [Nyall Dawson](https://north-road.com/) for the support.  

Founded by [Lepida SPA](https://www.lepida.it/), developed by Matteo Ghetta, [Faunalia](https://www.faunalia.eu/)
